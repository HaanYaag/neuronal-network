package math;

import java.util.Random;

/**
 * operation
 * 
 *
 */
public class matrix {
	
	public static int[] MatrixtoArray(int[][] A) {
	
		int [] one;
		
		int len = 0;
		for(int i = 0;i < A.length;i++){
		len += A[i].length;
		}
		
		
		one=new int[len];
		
		int index=0;
		
		for(int i=0;i<A.length;i++){
		for(int j=0;j<A[i].length;j++){
		
		one[index++]=A[i][j];
		}
		}
		
		

		
		return one;
	} 
	
	/**
	 * find the index(only row) of maxnumber in matrix
	 * 
	 * @param int[][]
	 * @return int
	 */
	public static int maxValueRow(int[][] M) {
		
	
		int max_a = 0;
		for(int i=0;i<M.length;i++) {
			for(int j=0;j<M[0].length;j++)
			{
			if(M[i][j]>M[i][max_a]) {
				//max = M[i][j];
				max_a = i;
				                    }
			}
		
	                                    }
		return max_a+1;
	}
	/**
	 * find the index(only row) of maxnumber in matrix
	 * 
	 * @param float[][]
	 * @return int
	 */
	public static int maxValueRow(float[][] M) {
		
		
		
	
		int max_a = 0;
		for(int i=0;i<M.length;i++) {
			for(int j=0;j<M[0].length;j++)
			{
			if(M[i][j]>M[i][max_a]) {
				//max = M[i][j];
				max_a = i;
				                    }
			}
		
	                                    }
		return max_a+1;
	}
	/**
	 * add two matrixes a+b
	 * 
	 * @param A
	 * @return
	 */
	public static int[][] minus(int[][] A, int[][] B) throws Exception {
		//determine if the rows and columns of the A matrix are equal to B matrix
		
		if (A[0].length != B[0].length||A.length != B.length) {
			throw new Exception("rows and columns are not equal");
		}
		int c[][] = new int[A.length][B[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < B[i].length; j++) {
				c[i][j]=A[i][j]-B[i][j];
			}
		}
		return c;
	}
	public static float[][] minus(float[][] A, float[][] B) throws Exception {
		//determine if the rows and columns of the A matrix are equal to B matrix
	
		if (A[0].length != B[0].length||A.length != B.length) {
			throw new Exception("rows and columns are not equal");
		}
		float c[][] = new float[A.length][B[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < B[i].length; j++) {
				c[i][j]=A[i][j]-B[i][j];
			}
		}
		return c;
	}
	
	/**
	 * @param float[][]
	*/
	public  void print(float[][]Matrix) {
		
		for (int i = 0; i < Matrix.length; i++) {
			for (int j = 0; j < Matrix[0].length; j++) {
				System.out.print(Matrix[i][j] + " ");
			}
			System.out.println();
		}
		
	}
	/**
	 * @param int[][]
	*/
public static  void print(int[][]Matrix) {
		
		for (int i = 0; i < Matrix.length; i++) {
			for (int j = 0; j < Matrix[0].length; j++) {
				System.out.print(Matrix[i][j] + " ");
			}
			System.out.println();
		}
		
	}
/**
 * sigmoid 
 * 
 * @param int[][]A
 * @return float[][]neu
 */
public static float[][] sigmoid(float[][] A) {
	float[][] neu = new float[A[0].length][A.length];
	for (int i = 0; i < neu.length; i++) {
		for (int j = 0; j < neu[0].length; j++) {
			neu[i][j] = (float) (1.0 / ( 1 + Math.exp(-A[i][j])));;
		}
	}
	return neu;
}
public static float[][] sigmoid(int[][] A) {
	float[][] neu = new float[A[0].length][A.length];
	for (int i = 0; i < neu.length; i++) {
		for (int j = 0; j < neu[0].length; j++) {
			neu[i][j] = (float) (1.0 / ( 1 + Math.exp(-A[i][j])));;
		}
	}
	return neu;
}


	/**
	 * create a matrix, Rows and columns are random numbers between 0 and 1
	 * 
	 * @param length1 length2
	 * @return float[][]
	 */
	 public static float[][] randomMatrix(int length1,int length2){//Row length1 and column length2
		  
		float[][]a1 = new float[length1][length2];
		 
			Random ran=new Random();
		  for(int i=0;i<a1.length;i++){
		   for(int j=0;j<a1[0].length;j++){
		    a1[i][j]=ran.nextFloat();
		   }
		  }
		return a1;
		  
		 }

	
	/**
	 * fill the matrix, fill in 0 after the insufficient rows
	 * 
	 * @param M
	 * @return
	 */
	public static int[][] fillMatrix(int[][] M) {
		int ml = 0;// Longest row
		for (int i = 0; i < M.length; i++) {
			ml = ml < M[i].length ? M[i].length : ml;
		}
		int Nm[][] = new int[M.length][ml];
		for (int i = 0; i < M.length; i++) {
			for (int j = 0; j < M[i].length; j++) {
				Nm[i][j] = M[i][j];
			}
		}
		return Nm;
	}
	public static float[][] fillMatrix(float[][] M) {
		int ml = 0;// Longest row
		for (int i = 0; i < M.length; i++) {
			ml = ml < M[i].length ? M[i].length : ml;
		}
		float Nm[][] = new float[M.length][ml];
		for (int i = 0; i < M.length; i++) {
			for (int j = 0; j < M[i].length; j++) {
				Nm[i][j] = M[i][j];
			}
		}
		return Nm;
	}

	/**
	 * multiplication A*B
	 * 
	 * @param A
	 * @param B
	 * @return
	 * @throws Exception
	 */
	public static int[][] multiplication(int[][] A, int[][] B) throws Exception {
		// determine if the columns of the A matrix are equal to the rows of B matrix
		
		if (A[0].length != B.length) {
			throw new Exception("the columns of the A matrix and the rows of B matrixcan are not equal");
		}
		int C[][] = new int[A.length][B[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < B[i].length; j++) {
				for (int k = 0; k < A[i].length; k++) {
					C[i][j] += A[i][k] * B[k][j];
				}
			}
		}
		return C;
	}
	public static float[][] multiplication(float[][] A, float[][] B) throws Exception {
		// determine if the columns of the A matrix are equal to the rows of B matrix
		A = fillMatrix(A);
		B = fillMatrix(B);
		if (A[0].length != B.length) {
			throw new Exception("the columns of the A matrix and the rows of B matrixcan are not equal");
		}
		float C[][] = new float[A.length][B[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < B[i].length; j++) {
				for (int k = 0; k < A[i].length; k++) {
					C[i][j] += A[i][k] * B[k][j];
				}
			}
		}
		return C;
	}

	public static float[][] matrix_addNum(float[][] a, float num){
		int cols=a[0].length;
		int rows=a.length;
		for(int i = 0; i != rows; i++){
			for(int p = 0; p != cols; p++){
				a[i][p]+=num;
			}
		}
	return a;
	}

	
	/**
	 * add two matrixes a+b
	 * 
	 * @param A
	 * @return
	 */
	public static int[][] add(int[][] A, int[][] B) throws Exception {
		//determine if the rows and columns of the A matrix are equal to B matrix
		
		if (A[0].length != B[0].length||A.length != B.length) {
			throw new Exception("rows and columns are not equal");
		}
		int c[][] = new int[A.length][B[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < B[i].length; j++) {
				c[i][j]=A[i][j]+B[i][j];
			}
		}
		return c;
	}
	public static float[][] add(float[][] A, float[][] B) throws Exception {
		//determine if the rows and columns of the A matrix are equal to B matrix
	
		if (A[0].length != B[0].length||A.length != B.length) {
			throw new Exception("rows and columns are not equal");
		}
		float c[][] = new float[A.length][B[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < B[i].length; j++) {
				c[i][j]=A[i][j]+B[i][j];
			}
		}
		return c;
	}
	/**
	 * Test function
	 */
	public static void main(String[] args) {
		int A[][] = { { 1, 2,6}, { 3, 4 ,3},{ 8, 2 ,3}};
		int B[][] = { { 3, 2,9}, { 3, 2,5 }, { 3, 4 ,3}};
		try {
			System.out.println("***********multify***********");
			int C[][] = multiplication(A, B);// multify
			for (int i = 0; i < C.length; i++) {
				for (int j = 0; j < C[0].length; j++) {
					System.out.print(C[i][j] + " ");
				}
				System.out.println();
			}
			System.out.println("***********maxValueRow***********");
			int num=maxValueRow(A);
			System.out.println(num);
			
			System.out.println("***********add***********");
			int d[][] = add(A, B);// add
			for (int i = 0; i < d.length; i++) {
				for (int j = 0; j < d[0].length; j++) {
					System.out.print(d[i][j] + " ");
				}
				System.out.println();
			}
			System.out.println("*********sigmoid*************");//sigmoid
			float sig[][] = sigmoid(B);
			for (int i = 0; i < sig.length; i++) {
				for (int j = 0; j < sig[0].length; j++) {
					System.out.print(sig[i][j] + " ");
				}
				System.out.println();
			}
			System.out.println("***********minus***********");//minus
			int min[][]=minus(A,B);
		   print(min);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		 System.out.println("**********mtoa************");
		 int[]neu=MatrixtoArray(B);
		 for(int n=0;n<neu.length;n++){
			 System.out.print(neu[n]+",");
			 }
			 }
		
	
		
		
		 
	}
	
	
	

	
	