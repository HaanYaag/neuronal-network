import math.*;
/**
 * @author: Han Yang
 * @date: 30.06.2020
 */
public class Network {
    private float[] weight;
    private float learningRate;
    private int epochs;
    //!should be [const]!
    private int sizeImage;  // this is the amount of nodes in Input Layer, the total pixel, 
    private float[][] weightInput2Hidden; // (total pixel, amount of nodes in hidden layer)
    private float[][] weightHidden2Output; // (hidden layer, amount of nodes in output layer)

    /**
     * use this method to USE the prediction 
     * @param input
     */
    public void feedForward(int[][] input){
        //check the size of input value
        if(input.lenth*input[0].lenth != sizeImage){
            System.out.println("**** you didn't set sizeImage or this image has wrong size ****");
            return;
        }
        // initialize the un-trained network
        if(this.weightInput2Hidden == null || this.weightHidden2Output == null){
            this.weightInput2Hidden = matrix.randomMatrix(numberOfHidden, sizeImage); 
            this.weightHidden2Output = matrix.randomMatrix(sizeImage, sizeImage); 
        }

        System.out.println("- input layer\n");
        matrix.print(input);

        float[][] hiddenLayer = matrix.sigmoid(matrix.multiplication(this.weightInput2Hidden,inputLayer));
        System.out.println("- hidden layer\n");
        matrix.print(hiddenLayer);
        
        float[][] outputLayer = matrix.sigmoid(matrix.multiplication(this.weightHidden2Output,hiddenLayer));
        System.out.println("- output layer\n");
        matrix.print(outputLayer);

        int realOutput = matrix.getRealOutput(outputLayer);
        System.out.println(String.format("the prediction is %d",realOutput));
    }

    /**
     * train the model
     * @param inputData
     * 
     * didn't finish yet!
     */
    public void train(List<Data> trainData) {
        //get the size of pixels of an image, x*y
        this.sizeImage = trainData[0].getInput().getSize(); 

        /**
         * weightInput2Hidden looks like
         * [w11 w12 w13]   how many row = how many nodes in Hidden Layer
         * [w21 w22 w23]   how many column = how many nodes in Input Layer
         */
        this.weightInput2Hidden = matrix.randomMatrix(numberOfHidden, sizeImage); 
        /**
         * weightHidden2Output looks like
         * [w11 w12 w13]   how many row = how many nodes in Output Layer
         * [w21 w22 w23]   how many column = how many nodes in Hidden Layer
         */
        this.weightHidden2Output = matrix.randomMatrix(sizeImage, sizeImage); 

        for (int i = 0; i < epochs; i++) { // whole training data run epochs times
            System.out.println(String.format("training the [%d]. time\n", i));
            for (int m = 0; m < trainData.size(); m++) {// each data runs once 
                System.out.println(String.format("- training the [%d/%d]. input date\n", m,trainData.size()));
                /**
                 * input Layer should looks like
                 * [pixel 1]    row: how many pixels totally
                 * [pixel 2]    column: 1
                 * [...]
                 * [pixel n]
                 */
                int[][] inputLayer = DataReader.inputToMatrix(trainData.getInput(m));
                System.out.println("- input layer\n");
                matrix.print(inputLayer);
                /**
                 * [h1]         [w11 w12 w13 ... w1n]      [x1]
                 * [h2]     =   [w21 w22 w23 ... w2n]  *   [x2]
                 * [...]        [...]                      [...]
                 * [hn]         [wn1 wn2 wn3 ... wnn]      [xn]
                 * H = W * I, W: weight matrix (Left) , I: input nodes (Right)
                 */
                float[][] hiddenLayer = matrix.sigmoid(matrix.multiplication(weightInput2Hidden,inputLayer));
                System.out.println("- hidden layer\n");
                matrix.print(hiddenLayer);
                 /**
                 * [O1]         [w11 w12 w13 ... w1n]      [h1]
                 * [O2]     =   [w21 w22 w23 ... w2n]  *   [h2]
                 * [...]        [...]                      [...]
                 * [On]         [wn1 wn2 wn3 ... wnn]      [hn]
                 * O = W * H, W: weight matrix (Left) , H: hidden nodes (Right)
                 */
                float[][] outputLayer = matrix.sigmoid(matrix.multiplication(weightHidden2Output,hiddenLayer));
                System.out.println("- output layer\n");
                matrix.print(outputLayer);
                /**
                 * this is the result for THIS input, not for a list of input
                 * it looks like
                 * [e1]
                 * [e2]
                 * [...]
                 * [en] same row as output, and 1 column
                 */
                float[][] outputLayerError = matrix.minus(trainData.getOutput(m),this.outputLayer);
                // another Idea to deal with error (T-O)ˆ2, 
                // T is the answer of the input value, 
                // O is the output value
                // with ˆ2 it could be always positive
                // and make error sensitive with the step
                // bigger error would be greater, smaller error would be smaller

                //float[][] outputLayerError = matrix.quadrat(matrix.minus(trainData.getOutput(m),this.outputLayer));
                System.out.println(String.format("- error the [%d/%d] input \n",m,trainData.size()));
                matrix.print(outputLayerError);
            }// for m < trainData.size()
        }// for i < epochs
    } // void train

    /**
     * the greatest value means the great possibility of a prediction
     * @param M
     * @return
     */
    public int getRealOutput(float[][] M){
         return matrix.maxValueRow(M); //return the index of the maximum value from the Output value
    }


} //Network