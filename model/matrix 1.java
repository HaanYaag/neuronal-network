package math;

import java.util.Random;

/**
 * operation
 * 
 *
 */
public class matrix {
	/**
	 * @param float[][]
	*/
	public  void print(float[][]Matrix) {
		
		for (int i = 0; i < Matrix.length; i++) {
			for (int j = 0; j < Matrix[0].length; j++) {
				System.out.print(Matrix[i][j] + " ");
			}
			System.out.println();
		}
		
	}
	/**
	 * @param int[][]
	*/
public  void print(int[][]Matrix) {
		
		for (int i = 0; i < Matrix.length; i++) {
			for (int j = 0; j < Matrix[0].length; j++) {
				System.out.print(Matrix[i][j] + " ");
			}
			System.out.println();
		}
		
	}
/**
 * sigmoid 
 * 
 * @param int[][]A
 * @return float[][]neu
 */
public static float[][] sigmoid(int[][] A) {
	float[][] neu = new float[A[0].length][A.length];
	for (int i = 0; i < neu.length; i++) {
		for (int j = 0; j < neu[0].length; j++) {
			neu[i][j] = (float) (1.0 / ( 1 + Math.exp(-A[i][j])));;
		}
	}
	return neu;
}


	/**
	 * create a matrix, Rows and columns are random numbers between 0 and 1
	 * 
	 * @param length1 length2
	 * @return float[][]
	 */
	 public static double[][] randomMatrix(int length1,int length2){//Row length1 and column length2
		  
		  double[][]a1 = new double[length2][length1];
		 
			Random ran=new Random();
		  for(int i=0;i<a1.length;i++){
		   for(int j=0;j<a1[0].length;j++){
		    a1[i][j]=ran.nextFloat();
		   }
		  }
		return a1;
		  
		 }

	
	/**
	 * fill the matrix, fill in 0 after the insufficient rows
	 * 
	 * @param M
	 * @return
	 */
	public static int[][] fillMatrix(int[][] M) {
		int ml = 0;// Longest row
		for (int i = 0; i < M.length; i++) {
			ml = ml < M[i].length ? M[i].length : ml;
		}
		int Nm[][] = new int[M.length][ml];
		for (int i = 0; i < M.length; i++) {
			for (int j = 0; j < M[i].length; j++) {
				Nm[i][j] = M[i][j];
			}
		}
		return Nm;
	}

	/**
	 * multiplication A*B
	 * 
	 * @param A
	 * @param B
	 * @return
	 * @throws Exception
	 */
	public static int[][] multiplication(int[][] A, int[][] B) throws Exception {
		// determine if the columns of the A matrix are equal to the rows of B matrix
		A = fillMatrix(A);
		B = fillMatrix(B);
		if (A[0].length != B.length) {
			throw new Exception("the columns of the A matrix and the rows of B matrixcan are not equal");
		}
		int C[][] = new int[A.length][B[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < B[i].length; j++) {
				for (int k = 0; k < A[i].length; k++) {
					C[i][j] += A[i][k] * B[k][j];
				}
			}
		}
		return C;
	}

	/**
	 * transpose a matrix
	 * 
	 * @param A
	 * @return
	 */
	public static int[][] transposed(int[][] A) {
		A = fillMatrix(A);
		int[][] AT = new int[A[0].length][A.length];
		for (int i = 0; i < AT.length; i++) {
			for (int j = 0; j < AT[0].length; j++) {
				AT[i][j] = A[j][i];
			}
		}
		return AT;
	}
	
	/**
	 * add two matrixes a+b
	 * 
	 * @param A
	 * @return
	 */
	public static int[][] add(int[][] A, int[][] B) throws Exception {
		//determine if the rows and columns of the A matrix are equal to B matrix
		A = fillMatrix(A);
		B = fillMatrix(B);
		if (A[0].length != B[0].length||A.length != B.length) {
			throw new Exception("rows and columns are not equal");
		}
		int c[][] = new int[A.length][B[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < B[i].length; j++) {
				c[i][j]=A[i][j]+B[i][j];
			}
		}
		return c;
	}
	
	/**
	 * Test function
	 */
	public static void main(String[] args) {
		int A[][] = { { 1, 2, 0 }, { 3, 4, 0 }, { 5, 6, 0 } };
		int B[][] = { { 1, 2, 3}, { 3, 2, 3 }, {9 , 4, 1} };
		try {
			int C[][] = multiplication(A, B);// multify
			for (int i = 0; i < C.length; i++) {
				for (int j = 0; j < C[0].length; j++) {
					System.out.print(C[i][j] + " ");
				}
				System.out.println();
			}
			System.out.println("**********************");//sigmoid
			float sig[][] = sigmoid(B);
			for (int i = 0; i < sig.length; i++) {
				for (int j = 0; j < sig[0].length; j++) {
					System.out.print(sig[i][j] + " ");
				}
				System.out.println();
			}
			
			System.out.println("**********************");
			int d[][] = add(A, B);// add
			for (int i = 0; i < d.length; i++) {
				for (int j = 0; j < d[0].length; j++) {
					System.out.print(d[i][j] + " ");
				}
				System.out.println();
			}
			System.out.println("**********************");
			int CT[][] = transposed(C);// transpose
			for (int i = 0; i < CT.length; i++) {
				for (int j = 0; j < CT[0].length; j++) {
					System.out.print(CT[i][j] + " ");
				}
				System.out.println();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		 double[][]d=randomMatrix(3,4) ;//random
		 System.out.println("**********************");
		  for(int i=0;i<d.length;i++){
		   for(int j=0;j<d[0].length;j++){
		    System.out.print(d[i][j]+" ");
		    }
		   System.out.println("\n");
		   }
		
	}
	
	
	
}