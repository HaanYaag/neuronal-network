public class Data{
    int[] input;
    int output; //which class is it

    /**
     * Data has an input and an output
     * input is an array with character
     * output represents if this data is a part of this class
     * @param input
     * @param output
     */
    public Data(int[] input, int output){
        this.input = input;
        this.output = output;
    }
    public int[] getInput(){
        return this.input;
    }
    public int getOutput(){
        return this.output;
    }


}