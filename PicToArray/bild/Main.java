import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.awt.*;
import java.awt.image.BufferedImage;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Main {
    public static void main(String[] args) {
        // pic2ArrFileIO();

        // pic2ArrBufferedImage();
        image2array2d();

    }
    /**
     * We save the image into array, not the file of picture.
     * 
     * in our tast, the GUI output only the image with two kind of pixiv: (painted , not painted)
     * it is actually binary image
     * so the first step we change the image into binary image
     * and then save the binary value into an array
     * (-16777216 is black), (-1 is white)
     * 
     * BufferedImage -> BufferedImage(TYPE_BYTE_BINARY) -> getRGB()
     */
    public static void image2array2d() {
        String path = "test.jpeg";
        try {
            BufferedImage bufferedImage = ImageIO.read(new File(path));

            //save in a binary image
            //create a binary image
            BufferedImage binarImage = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.TYPE_BYTE_BINARY);
            //save value into new image
            for (int i = 0; i < bufferedImage.getHeight(); i++) {
                for (int j = 0; j < bufferedImage.getWidth(); j++) {
                    int rgb = bufferedImage.getRGB(j, i);// int getRGB(int x,int y)
		            binarImage.setRGB(j, i, rgb);// int getRGB(int x,int y)
                }
            }
            //save the new image down
            File file2 = new File("test_binary.png");// create a new pic
            ImageIO.write(binarImage,"png",file2);

            // save the binar value into array[][]
            int[][] data = new int[bufferedImage.getWidth()][bufferedImage.getHeight()];
            String str = "";
            for (int i = 0; i < binarImage.getHeight(); i++) {
                for (int j = 0; j < binarImage.getWidth(); j++) {
                    if(binarImage.getRGB(j, i)==-16777216){ // int getRGB(int x,int y)
                        data[j][i] = 1; //black
                    }
                    else if(binarImage.getRGB(j, i)==-1){// int getRGB(int x,int y)
                        data[j][i] = 0; //white
                    }
                    // data[i][j] = binarImage.getRGB(j, i);// int getRGB(int x,int y)
                    str =str + data[j][i];
                }
                str = str+'\n';
            }
            System.out.printf(str);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * File
     * BufferedImage
     * ImageIO
     * .read()
     * .write()
     * 
     * ByteArrayOutputStream
     * .toByteArray()
     * 
     * StringBuilder
     * .append()
     * 
     * File -> ImageIO -> BufferedImage -> ByteArrayOutputStream -> byte[]
     */

    public static void pic2ArrBufferedImage() {
        String path = "test.png";
        File file = new File(path);
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, "jpg", byteArrayOutputStream); //write the image into byteArrayOutputStream

            byte[] b = byteArrayOutputStream.toByteArray(); //convert the byteArrayOutputStream into byte

            StringBuilder str = new StringBuilder();    //save byte into str
            for (byte bs : b) {
                str.append(Integer.toBinaryString(bs)); // convert into binar array
            }
        
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * FileInputStream .available() .read()
     * 
     * File FileOutputStream .write() .flush(); .close();
     * 
     * file -> FileInputStream -> byte[]
     */

    public static void pic2ArrFileIO() {
        String path = "test.png";

        File file = new File(path); // File read image
        FileInputStream fileInput;

        try {
            fileInput = new FileInputStream(file); // FileInputStream read File

            byte[] b;
            b = new byte[fileInput.available()]; // create the byte[]

            fileInput.read(b); // put image in byte[] b

            StringBuilder str = new StringBuilder();
            for (byte bs : b) {
                str.append(Integer.toBinaryString(bs)); // convert into binar array
            }

            System.out.println(str);

            File file2 = new File("test2.png");// create a new pic
            FileOutputStream fileOutput = new FileOutputStream(file2);
            fileOutput.write(b); // save b into the pic
            fileOutput.flush();
            fileOutput.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
