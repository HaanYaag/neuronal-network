import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.awt.*;
import java.awt.image.BufferedImage;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class cut {
    public static void main(String[] args)throws Exception {
        // cutImage50Array50();
        String input = "heart_b400.png";
        BufferedImage bufferedImage = ImageIO.read(new File(input));
        BufferedImage output = cutSideSkala50(bufferedImage);

        String output_skala = "heart_b400_cut_skala_newtest.png";
        ImageIO.write(output, "png", new FileOutputStream(output_skala));
    }
    /**
     * 1. select area -> x,y area
     * 2. save in new BufferedImage_cut [pic]
     * 3. skala BufferedImage_cut to 50*50 [pic]
     * (-16777216 is black), (-1 is white)
     */
    public static BufferedImage cutSideSkala50 (BufferedImage bufferedImage)throws Exception  {
        // binary image
        BufferedImage binarImage = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.TYPE_BYTE_BINARY);
        //save value into new image
            for (int i = 0; i < bufferedImage.getHeight(); i++) {
                for (int j = 0; j < bufferedImage.getWidth(); j++) {
                    int rgb = bufferedImage.getRGB(j, i);// int getRGB(int x,int y)
		            binarImage.setRGB(j, i, rgb);// int getRGB(int x,int y)
                }
            }

            //select area
            int x_big = 0;
            int x_small = 0;
            int y_big = 0;
            int y_small = 0;
            for (int i = 0; i < binarImage.getHeight(); i++) {
                for (int j = 0; j < binarImage.getWidth(); j++) {
                    if(binarImage.getRGB(j, i) == -16777216){ // int getRGB(weight,height)
                        if(x_small == 0 || x_small > j){
                            x_small = j;
                        }
                        if(x_big < j){
                            x_big = j;
                        }
                        if(y_big < i){
                            y_big = i;
                        }
                        if(y_small == 0 || y_small > i){
                            y_small = i;
                        }
                    }
                }
                System.out.printf(String.format("x_small:%d,x_big:%d,y_big:%d,y_small:%d\n",x_small,x_big,y_big,y_small));
            }

            int x_diff = x_big - x_small;
            int y_diff = y_big - y_small;
            int[][] data = new int[x_diff][y_diff];

            BufferedImage cutImage = new BufferedImage(x_diff, y_diff, BufferedImage.TYPE_BYTE_BINARY);

            String str = "";
            for (int i = 0; i < binarImage.getHeight(); i++) {
                if(i < y_big && i > y_small){
                    for (int j = 0; j < binarImage.getWidth(); j++) {
                        if(j < x_big && j > x_small ){

                            int rgb_cut = binarImage.getRGB(j, i);
                            cutImage.setRGB(j-x_small, i-y_small, rgb_cut);
                        } // j range
                }// for j
            }//x range
            } // for i

            File file3 = new File(output_cut);// save cut on computer
            ImageIO.write(cutImage,"png",file3);

            // skala
            Image image_skala = cutImage.getScaledInstance(50, 50, Image.SCALE_DEFAULT);
            BufferedImage output_cut_skala = new BufferedImage(50, 50, BufferedImage.TYPE_BYTE_BINARY);
            Graphics g = output_cut_skala.getGraphics();
            g.drawImage(image_skala, 0, 0, null);
            g.dispose();
            // ImageIO.write(output_cut_skala, "png", new FileOutputStream(output_skala));
            return output_cut_skala;

    }


    
    /************ Testing ************/
    public static void cutImage50Array50() {

        String input = "heart_b400.png";
        // String input = "lena_binary.png";
        // String output = "apple_b300.png";
        String output_cut = "heart_b400_cut.png";
        String output_skala = "heart_b400_cut_skala.png";
        try {
            BufferedImage bufferedImage = ImageIO.read(new File(input));

            //save in a binary image
            //create a binary image
            BufferedImage binarImage = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.TYPE_BYTE_BINARY);
            //save value into new image
            for (int i = 0; i < bufferedImage.getHeight(); i++) {
                for (int j = 0; j < bufferedImage.getWidth(); j++) {
                    int rgb = bufferedImage.getRGB(j, i);// int getRGB(int x,int y)
		            binarImage.setRGB(j, i, rgb);// int getRGB(int x,int y)
                }
            }

            int x_big = 0;
            int x_small = 0;
            int y_big = 0;
            int y_small = 0;
            for (int i = 0; i < binarImage.getHeight(); i++) {
                for (int j = 0; j < binarImage.getWidth(); j++) {
                    if(binarImage.getRGB(j, i) == -16777216){ // int getRGB(weight,height)
                        if(x_small == 0 || x_small > j){
                            x_small = j;
                        }
                        if(x_big < j){
                            x_big = j;
                        }
                        if(y_big < i){
                            y_big = i;
                        }
                        if(y_small == 0 || y_small > i){
                            y_small = i;
                        }
                    }
                }
                System.out.printf(String.format("x_small:%d,x_big:%d,y_big:%d,y_small:%d\n",x_small,x_big,y_big,y_small));
            }


            int x_diff = x_big - x_small;
            int y_diff = y_big - y_small;
            int[][] data = new int[x_diff][y_diff];

            BufferedImage cutImage = new BufferedImage(x_diff, y_diff, BufferedImage.TYPE_BYTE_BINARY);

            String str = "";
            for (int i = 0; i < binarImage.getHeight(); i++) {
                if(i < y_big && i > y_small){
                    for (int j = 0; j < binarImage.getWidth(); j++) {
                        if(j < x_big && j > x_small ){

                            int rgb_cut = binarImage.getRGB(j, i);
                            cutImage.setRGB(j-x_small, i-y_small, rgb_cut);

                            /* save into array
                            if(binarImage.getRGB(j, i)==-16777216){ // int getRGB(int x,int y)
                                data[j-x_small][i-y_small] = 1; //black
                            }
                            else if(binarImage.getRGB(j, i)==-1){// int getRGB(int x,int y)
                                data[j-x_small][i-y_small] = 0; //white
                            }
                            // str =str + data[j-x_small][i-y_small]; // print 
                            */
                        } // j range
                }// for j
                // str = str+'\n';
            }//x range
                
                // System.out.printf(String.format("x_small:%d,x_big:%d,y_big:%d,y_small:%d\n",x_small,x_big,y_big,y_small));
            } // for i
            // System.out.printf(str);
            File file3 = new File(output_cut);// create a new pic
            ImageIO.write(cutImage,"png",file3);


            // skala
            Image image_skala = cutImage.getScaledInstance(50, 50, Image.SCALE_DEFAULT);
            BufferedImage tag = new BufferedImage(50, 50, BufferedImage.TYPE_BYTE_BINARY);
            Graphics g = tag.getGraphics();
            g.drawImage(image_skala, 0, 0, null);
            g.dispose();
            ImageIO.write(tag, "png", new FileOutputStream(output_skala));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
