import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import javax.imageio.ImageIO;

public class cut_abgabe {
    public static void main(String[] args) throws Exception {
        // cutImage50Array50();
        String input = "apple_b300.png";
        BufferedImage bufferedImage = ImageIO.read(new File(input));
        BufferedImage output = cutSideSkala50(bufferedImage);

        String output_skala = "apple_b300_cut_skala_newtest.png";
        ImageIO.write(output, "png", new FileOutputStream(output_skala));
    }

    /**
     * 1. select area -> x,y area
     * 2. save in new BufferedImage_cut [pic]
     * 3. skala BufferedImage_cut to 50*50 [pic]
     * (-16777216 is black), (-1 is white)
     */

    /**
     * this function would cut the white side of an image
     * firstly convert image into binary image, 
     * then select the area without white side and save image, then skalieren to 50*50
     * @param bufferedImage an image
     * @return BufferedImage output_cut_skala
     * @throws Exception
     */
    public static BufferedImage cutSideSkala50 (BufferedImage bufferedImage)throws Exception  {
        // binary image
        BufferedImage binarImage = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.TYPE_BYTE_BINARY);
        //save value of binary image
        for (int i = 0; i < bufferedImage.getHeight(); i++) {
            for (int j = 0; j < bufferedImage.getWidth(); j++) {
                int rgb = bufferedImage.getRGB(j, i);// int getRGB(int x,int y)
		        binarImage.setRGB(j, i, rgb);// int getRGB(int x,int y)
            }
        }

        //select area
        int x_big = 0;
        int x_small = 0;
        int y_big = 0;
        int y_small = 0;
        for (int i = 0; i < binarImage.getHeight(); i++) {
            for (int j = 0; j < binarImage.getWidth(); j++) {
                if(binarImage.getRGB(j, i) == -16777216){ // int getRGB(weight,height)
                    if(x_small == 0 || x_small > j){
                        x_small = j;
                    }
                    if(x_big < j){
                        x_big = j;
                    }
                    if(y_big < i){
                        y_big = i;
                    }
                    if(y_small == 0 || y_small > i){
                        y_small = i;
                    }
                }
            }
        }
        // show the result of x and y
        System.out.printf(String.format("x_small:%d,x_big:%d,y_big:%d,y_small:%d\n",x_small,x_big,y_big,y_small));

        // select image, save as [BufferedImage cutImage]
        int x_diff = x_big - x_small;
        int y_diff = y_big - y_small;
        int[][] data = new int[x_diff][y_diff];
        BufferedImage cutImage = new BufferedImage(x_diff, y_diff, BufferedImage.TYPE_BYTE_BINARY);
        for (int i = 0; i < binarImage.getHeight(); i++) {
            if(i < y_big && i > y_small){
                for (int j = 0; j < binarImage.getWidth(); j++) {
                    if(j < x_big && j > x_small ){
                        int rgb_cut = binarImage.getRGB(j, i);
                        cutImage.setRGB(j-x_small, i-y_small, rgb_cut);
                    } // j range
                }// for j
            }//x range
        } // for i

        // save cut on computer
        //File file3 = new File("output_cut.png");
        //ImageIO.write(cutImage,"png",file3);

        // skala to 50*50, save as [BufferedImage output_cut_skala]
        Image image_skala = cutImage.getScaledInstance(50, 50, Image.SCALE_DEFAULT);
        BufferedImage output_cut_skala = new BufferedImage(50, 50, BufferedImage.TYPE_BYTE_BINARY);
        Graphics g = output_cut_skala.getGraphics();
        g.drawImage(image_skala, 0, 0, null);
        g.dispose();
        // ImageIO.write(output_cut_skala, "png", new FileOutputStream("output_skala"));
        return output_cut_skala;

    }
}
