import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.awt.*;
import java.awt.image.BufferedImage;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Skala {
    public static void main(String[] args) {
        scaleToFile(40,40); //the new size 
    }

    public static void scaleToFile(int width, int height) {

        String input = "apple_b300.png";
        String output = "apple_b300_40.png";
        try {
            File destFile = new File(output);
            BufferedImage src = ImageIO.read(new File(input));
            Image image = src.getScaledInstance(width, height, Image.SCALE_DEFAULT);
            BufferedImage tag = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_BINARY);
            Graphics g = tag.getGraphics();
            g.drawImage(image, 0, 0, null);
            g.dispose();
            ImageIO.write(tag, "png", new FileOutputStream(destFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}