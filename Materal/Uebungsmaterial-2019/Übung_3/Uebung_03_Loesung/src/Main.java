import datareader.Data;
import datareader.DataReader;
import network.Network;
import weightsreaderwriter.WeightsReaderWriter;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static double NOISE = 0.1;
    private static String DATA = "data";

    /**
     * Main method to start the program.
     * Reads the arguments and asks the user what to do.
     * t: for training
     * l for loading matrices
     *
     * @param args
     */
    public static void main(String[] args) {

        readArgs(args);
        List<Data> trainData = DataReader.readData(DATA);

        Scanner input = new Scanner(System.in);
        System.out.println("Tippe 't' für Training, 'l' für das Laden der Gewichtsmatrizen aus einer Datei!");
        String cmd = input.nextLine();

        Network network = new Network();

        if (cmd.equals("t")) {
            System.out.println("Starte Training");
            network.train(trainData);
            int counter = 0;
            System.out.println("Trainingsergebnis: ");
            for (Data d : trainData)
                System.out.println("Klasse: " + counter++ + ": " + classify(trainData, network.feedForward(d)));
            WeightsReaderWriter.write(network);
        }
        else if (cmd.equals("l")){
            System.out.println("Lade Trainingsdaten");
            WeightsReaderWriter.read(network);
        }
        else
        return;

        System.out.println("Tippe 't' zum Testen!");
        cmd = input.nextLine();

        if (cmd.equals("t")) {
            int counter = 0;
            System.out.println("Testergebnis mit noise: ");
            if (cmd.equals("t"))
                for (Data d : DataReader.readNoiseData(DATA, NOISE))
                    System.out.println("Klasse: " + counter++ + ": " + classify(trainData, network.feedForward(d)));
        }

    }

    /**
     * Read the arguments:
     * -d <path>
     * -e <epochs>
     * -n <noise>
     *
     * @param args
     */
    private static void readArgs(String[] args) {
        for (int i = 0; i < args.length; i++){
            String cmd = args[i];
            if (cmd.equals("-d")){
                i++;
                DATA = args[i];
            }
            if (cmd.equals("-e")){
                i++;
                Network.EPOCHS = Integer.parseInt(args[i]);
            }
            if (cmd.equals("-n")){
                i++;
                NOISE = Double.parseDouble(args[i]);
            }
        }

        System.out.println("Übersicht:");
        System.out.println("Pfad zu Data: ./" + DATA);
        System.out.println("Anzahl Epochen: " + Network.EPOCHS);
        System.out.println("Noise: " + NOISE);
    }

    /**
     * Mapping from an array to the letter.
     *
     * @param trainData
     * @param floats
     * @return
     */
    private static String classify(List<Data> trainData, float[] floats) {

        float max = Float.MIN_VALUE;
        int index = -1;
        for (int i = 0; i < floats.length; i++)
            if (floats[i] > max) {
                index = i;
                max = floats[i];
            }
        return ""+trainData.get(index).getC();
    }
}
