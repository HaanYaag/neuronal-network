package datareader;

import math.Matrix;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class DataReader {

    private static final int CLASSES = 6;
    private static final String SPLIT = " ";

    /**
     * Reads all file from a folder and returns this as a List<Data>.
     *
     * @param path
     * @return
     */
    public static List<Data> readData(String path){

        List<Data> trainDataList = new ArrayList<Data>();
        int output = 0;
        File folder = new File(path);
        for (File file : folder.listFiles()){
            if (file.getName().equals(".DS_Store"))
                continue;
            try {
                List<String> lines = Files.readAllLines(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8);
                if (lines.isEmpty())
                    continue;
                float[] inputs = convertStringArrayToFloatArray(lines);
                Data data = new Data(inputs, getOutput(output++), file.getName().charAt(0));
                trainDataList.add(data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return trainDataList;
    }

    /**
     * Reads all file from a folder.
     * The returned list of data has a randomly added noise.
     *
     * @param path
     * @return
     */
    public static List<Data> readNoiseData(String path, double noise){

        List<Data> trainDataList = new ArrayList<Data>();
        int output = 0;
        File folder = new File(path);
        for (File file : folder.listFiles()){
            if (file.getName().equals(".DS_Store"))
                continue;
            try {
                List<String> lines = Files.readAllLines(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8);
                if (lines.isEmpty())
                    continue;
                float[] inputs = convertStringArrayToFloatArray(lines);

                Random random = new Random();
                // add noise
                for (int i = 0; i < noise*inputs.length; i++){
                    int j = random.nextInt(inputs.length);
                    inputs[j] = 1;
                }

                Data data = new Data(inputs, getOutput(output++), file.getName().charAt(0));
                trainDataList.add(data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return trainDataList;
    }

    /**
     * Helper method to add one 1 in an array.
     * This array represents a letter.
     *
     * @param i
     * @return
     */
    private static float[] getOutput(int i) {
        float[] output = new float[CLASSES];
        output[i] = 1;
        return output;
    }

    /**
     * Helper method to convert String lines to one float array.
     *
     * @param lines
     * @return
     */
    private static float[] convertStringArrayToFloatArray(List<String> lines) {
        int size = lines.get(0).split(SPLIT).length * lines.size();
        float[] inputs = new float[size];

        int i = 0;
        for (String line : lines)
            for (String s : line.split(SPLIT))
                inputs[i++] = Float.parseFloat(s);

        return inputs;
    }

    /**
     * Helper method to print a converted letter in a console.
     *
     * @param data
     */
    public static void printAlpha(Data data){
        int i = 1;
        for (float f : data.getInput()){

            if (f == 1.0f)
                System.out.print("1 ");
            else
                System.out.print("0 ");

            if ((i++ % 12) == 0)
                System.out.println("");
        }
        System.out.println("____________");
    }

    /**
     * Helper method to convert Data.input to Matrix.
     * @param data
     * @return
     */
    public static Matrix inputToMatrix(Data data) {

        Matrix m = new Matrix(1, data.getInput().length);
        for (int i = 0; i < data.getInput().length; i++)
            m.setValue(0, i, data.getInput()[i]);
        return m;
    }
}
