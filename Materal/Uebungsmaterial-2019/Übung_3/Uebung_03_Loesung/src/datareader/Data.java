package datareader;

public class Data {

    private float[] input;
    private float[] output;
    private final char c;

    public Data(float[] input, float[] output, char c) {
        this.input = input;
        this.output = output;
        this.c = c;
    }

    public float[] getInput() {
        return input;
    }

    public void setInput(float[] input) {
        this.input = input;
    }

    public float[] getOutput() {
        return output;
    }

    public void setOutput(float[] output) {
        this.output = output;
    }

    public char getC() {
        return c;
    }
}
