package math;

import java.util.Random;

public class Matrix {

    private float matrix[][];
    private int column;
    private int row;

    /**
     * @param rows    Number of Matrix' rows
     * @param columns Number of Matrix' columns
     */
    public Matrix(int rows, int columns) {
        this.row = rows;
        this.column = columns;
        this.matrix = new float[rows][columns];
        this.randomFeed(matrix, rows, columns);
    }

    public Matrix(float[][] matrixRes) {
        this.row = matrixRes.length;
        this.column = matrixRes[0].length;
        this.matrix = matrixRes;
    }


    /**
     * This method feeds the matrix with random values
     *
     * @param matrix  The matrix
     * @param rows    Number of Matrix' rows
     * @param columns Number of Matrix' columns
     */
    public void randomFeed(float matrix[][], int rows, int columns) {
        Random rnd = new Random();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                matrix[i][j] = rnd.nextFloat()*2-1;
            }
        }
    }

    /**
     * Get a new matrix which is transposed.
     *
     * @return
     */
    public Matrix transpose() {
        float[][] values = new float[column][row];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                values[j][i] = this.matrix[i][j];
            }
        }
        return new Matrix(values);
    }

    public float[] getRow(int i) {
        return this.matrix[i];
    }

    public float[][] getMatrix() {
        return matrix;
    }

    public void setRow(int i, float[] input) {
        this.matrix[i] = input;
    }

    public void setValue(int m, int n, float v) {
        this.matrix[m][n] = v;
    }

    public int getColumns() {
        return column;
    }

    public int getRows() {
        return row;
    }

    public float getValue(int i, int j) {
        return this.matrix[i][j];
    }

}
