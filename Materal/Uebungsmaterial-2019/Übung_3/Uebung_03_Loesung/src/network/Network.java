package network;

import datareader.Data;
import datareader.DataReader;
import helperclasses.MatrixOperations;
import math.Matrix;

import java.util.List;

public class Network {

    public static int EPOCHS = 10000;

    private Matrix weightsInputToHidden;
    private Matrix weightsHiddenToOutput;

    public Network(){
    }

    /**
     * Runs the backpropagation algorithm.
     * At first it creates two matrices with randomly generated elements.
     * In every iteration it runs over all train data and it feed the net forwardly.
     * Then it determines the error and update the weights after a backpropagation.
     *
     * @param trainData
     */
    public void train(List<Data> trainData) {
        // trainData: 6 x 144

        this.weightsInputToHidden = new Matrix(144, 10); // 144 x 10 -> 6x10
        this.weightsHiddenToOutput = new Matrix(10, 6); // 10 x 6 -> 6x6

        for (int i = 0; i < EPOCHS; i++) {

            for (int m = 0; m < trainData.size(); m++) {

                // feed forward
                Matrix inputLayer = DataReader.inputToMatrix(trainData.get(m)); // 1 x 144
                Matrix hiddenLayer = MatrixOperations.sigmoid(
                        MatrixOperations.matMult(inputLayer, this.weightsInputToHidden));
                Matrix outputLayer = MatrixOperations.sigmoid(
                        MatrixOperations.matMult(hiddenLayer, this.weightsHiddenToOutput)); // 1 x 6

                // calculate error
                Matrix outputLayerError = calcError(trainData.get(m).getOutput(), outputLayer.getMatrix()[0]);

                // backpropagation
                // in what direction is the target value? were we really sure? if so, don't change too much.
                Matrix outputLayerDelta = MatrixOperations.matMult(outputLayerError,
                        MatrixOperations.sigmoidDerivation(outputLayer));

                // how much did each hiddenlayer value contribute to the outputlayer error (according to the weights)?
                Matrix hiddenLayerError = MatrixOperations.matMult(outputLayerDelta.transpose(), this.weightsHiddenToOutput.transpose());

                // in what direction is the target hiddenlayer? were we really sure? if so, don't change too much.
                Matrix hiddenLayerDelta = MatrixOperations.matMult(hiddenLayerError.transpose(),
                        MatrixOperations.sigmoidDerivation(hiddenLayer));

                // update weights
                this.weightsHiddenToOutput = MatrixOperations.matAdd(this.weightsHiddenToOutput,
                        MatrixOperations.matMult(hiddenLayer.transpose(), outputLayerDelta.transpose()));
                this.weightsInputToHidden = MatrixOperations.matAdd(this.weightsInputToHidden,
                        MatrixOperations.matMult(inputLayer.transpose(), hiddenLayerDelta.transpose()));
            }
        }
    }

    /**
     * Returns the difference between the expected result and the predicted result.
     *
     * @param output
     * @param prediction
     * @return
     */
    private Matrix calcError(float[] output, float[] prediction) {
        Matrix error = new Matrix(output.length, 1);
        for (int i = 0; i < output.length; i++)
            error.setValue(i, 0, output[i] - prediction[i]);
        return error;
    }

    /**
     * Predict the class for an input.
     *
     * @param input
     * @return
     */
    public float[] feedForward(Data input){

        Matrix inputLayer = DataReader.inputToMatrix(input); // 1 x 144
        Matrix hiddenLayer = MatrixOperations.sigmoid(
                MatrixOperations.matMult(inputLayer, this.weightsInputToHidden));
        Matrix outputLayer = MatrixOperations.sigmoid(
                MatrixOperations.matMult(hiddenLayer, this.weightsHiddenToOutput)); // 1 x 6

        return outputLayer.getRow(0);
    }

    public Matrix getWeightsInputToHidden() {
        return weightsInputToHidden;
    }

    public Matrix getWeightsHiddenToOutput() {
        return weightsHiddenToOutput;
    }

    public void setWeightsInputToHidden(Matrix weightsInputToHidden) {
        this.weightsInputToHidden = weightsInputToHidden;
    }

    public void setWeightsHiddenToOutput(Matrix weightsHiddenToOutput) {
        this.weightsHiddenToOutput = weightsHiddenToOutput;
    }
}
