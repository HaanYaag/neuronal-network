package weightsreaderwriter;

import math.Matrix;
import network.Network;

import java.io.*;

public class WeightsReaderWriter {

    private static final String INPUT_TO_HIDDEN = "input_to_hidden.weights";
    private static final String HIDDEN_TO_OUTPUT = "hidden_to_output.weights";

    /**
     * Write the weight matrices to file.
     *
     * @param network
     */
    public static void write(Network network) {
        writeArray(INPUT_TO_HIDDEN, network.getWeightsInputToHidden().getMatrix());
        writeArray(HIDDEN_TO_OUTPUT, network.getWeightsHiddenToOutput().getMatrix());
    }

    /**
     * Read the weight matrices from file.
     *
     * @param network
     */
    public static void read(Network network) {
        float[][] ith = readArray(INPUT_TO_HIDDEN);
        float[][] hto = readArray(HIDDEN_TO_OUTPUT);
        network.setWeightsInputToHidden(new Matrix(ith));
        network.setWeightsHiddenToOutput(new Matrix(hto));
    }

    /**
     * Write a double array of float to a file f.
     *
     * @param f
     * @param array
     */
    private static void writeArray(String f, float[][] array) {
        try {
            File file = new File(f);
            if (!file.exists()) {
                file.delete();
                file.createNewFile();
            }
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f));
            oos.writeObject(array);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Read a double array of float from a file.
     *
     * @param f
     * @return
     */
    private static float[][] readArray(String f) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));
            float[][] array = (float[][])ois.readObject();
            return array;

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
