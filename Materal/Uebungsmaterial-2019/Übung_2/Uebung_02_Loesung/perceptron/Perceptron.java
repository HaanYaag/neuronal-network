package perceptron;

import datareader.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Perceptron {

    private float[] weights;
    private float learningrate;
    private int epochs;

    public Perceptron(float learningrate, int epochs){
        this.learningrate = learningrate;
        this.epochs = epochs;
    }

    /**
     * This method trains the perceptron for a given traindata and the trainoutput based on the
     * learning rate. The epochs are the max number of iterations.
     * If the accuracy is better then 0.95 then this method stops.
     *
     * @param traindata
     */
    public void train(List<Data> traindata) {
        int n = traindata.get(0).getInput().length;
        this.weights = new float[n];
        Random r = new Random();

        //initialize weights
        for (int i = 0; i < n; i++) {
            weights[i] = r.nextFloat();
        }

        // train
        for (int i = 0; i < epochs; i++) {

            List<Float> results = new ArrayList();
            for (int j = 0; j < traindata.size(); j++) {
                for (int k = 0; k < n; k++) {

                    float expected = traindata.get(j).getOutput();
                    float out = output(traindata.get(j).getInput());

                    float error = (expected - out);
                    results.add(error);
                    float delta = learningrate * error * traindata.get(j).getInput()[k];
                    weights[k] += delta;
                }
            }

            if (accuracy(results) >= 0.95)
                break;
        }
    }

    /**
     * Calculates the accuracy for one iteration.
     *
     * @param results
     * @return
     */
    private float accuracy(List<Float> results) {
        float accuracy = 0.0f;

        for (int i = 0; i < results.size(); i++)
            if (results.get(i) == 0)
                accuracy += 1;

        return accuracy / results.size();
    }

    /**
     * Predicts for a given input an output which is based on the weights.
     * @param input
     * @return
     */
    public int output(float[] input) {
        float sum = 0.0f;
        for (int i = 0; i < input.length; i++) {
            sum += weights[i] * input[i];
        }

        if (sum >= 0)
            return 1;
        else
            return 0;
    }
}
