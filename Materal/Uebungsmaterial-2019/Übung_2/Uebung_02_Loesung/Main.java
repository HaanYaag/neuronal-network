import datareader.DataReader;
import perceptron.Perceptron;

public class Main {

    private final static float TRAIN_DATA[][] = {
            {10.7f, 6.1f}, {4.9f, 3.4f}, {3.8f, 5.9f}, {1.7f, 4.7f},
            {8.9f, 2.6f}, {8.9f, 1.8f}, {8.8f, 1.9f}, {13.4f, 1.7f},
            {19.8f, 0.0f}, {18.9f, 0.1f}, {15.8f, 0.0f}, {12.1f, 1.4f},
            {8.6f, 3.8f}, {5.8f, 1.6f}, {2.3f, 2.9f}, {3.3f, 5.3f},
            {14.9f, 0.5f}, {18.8f, 0.3f}, {22.8f, 0.0f}, {27.3f, 0.3f},
            {30.7f, 0.0f}, {30.6f, 0.0f}, {27.6f, 0.0f}, {23.2f, 0.2f}};

    private final static int TRAIN_OUTPUT[] = {
            1,1,1,1,
            1,1,1,1,
            0,0,0,1,
            1,1,1,1,
            1,0,0,0,
            0,0,0,0};

    // testdata, which is not cointained in the traindata
    private final static float TEST_DATA[][] = {
            {11.2f, 2.1f}, {23.2f, 0.1f}, {16.0f, 0.0f}, {0.5f, 1.7f}, {14.9f, 1.7f}};

    public static void main(String[] args) {
        Perceptron p = new Perceptron(0.5f, 100);

        p.train(DataReader.readData(TRAIN_DATA, TRAIN_OUTPUT));

        for (float[] in : TEST_DATA)
            System.out.println("Temperatur: " + in[0] + ", Niederschlag: " + in[1] +
                    " -> " + ((p.output(in) == 1) ? "Schlechtes Wetter" : "Gutes Wetter") );

    }
}
