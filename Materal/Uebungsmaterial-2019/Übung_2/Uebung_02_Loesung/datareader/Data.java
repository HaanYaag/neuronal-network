package datareader;

public class Data {

    private float[] input;
    private float output;

    public Data(float[] input, float output) {
        this.input = input;
        this.output = output;
    }

    public float[] getInput() {
        return input;
    }

    public void setInput(float[] input) {
        this.input = input;
    }

    public float getOutput() {
        return output;
    }

    public void setOutput(float output) {
        this.output = output;
    }
}
