package datareader;

import java.util.ArrayList;
import java.util.List;

public class DataReader {

    /**
     * Returns a list with all traindata and his trainoutput as Data.
     *
     * @param traindata
     * @param trainOutput
     * @return list with all data
     */
    public static List<Data> readData(float[][] traindata, int[] trainOutput){
        List<Data> trainDataList = new ArrayList<Data>();

        for (int i = 0; i < traindata.length; i++)
            trainDataList.add(new Data(traindata[i], trainOutput[i]));

        return trainDataList;
    }
}
